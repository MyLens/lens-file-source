import MockFileSource from "../mock-file-source";
import JSONMapFileSource from "../../src/file-source/json-map-file-source";
import CachedFileSource from "../../src/file-source/cached-file-source";
import TransactionFileSource from "../../src/file-source/transaction-file-source"
import FileSource from '../../src/file-source/file-source';


test("Ensure delete is flushed to source.", async () => {
    const mSource = new MockFileSource(); 
    const cachedJsonFileSource = new JSONMapFileSource(new CachedFileSource(mSource, { autoFlushing: false })) 
    const indexFileSource = new TransactionFileSource(cachedJsonFileSource);

    const testPath = "some_data" 
    const testContent = "Some content"
    // This Create data item and force flush 
    await indexFileSource.putFile(testPath, testContent, true)
    await cachedJsonFileSource.flush(); 

    // Make sure that the correct value is in the other file source. 
    // This ensures that the data was actually written to the datastore. 
    const cachedJsonFileSource1 = new JSONMapFileSource(new CachedFileSource(mSource, { autoFlushing: false })) 
    const indexFileSource1 = new TransactionFileSource(cachedJsonFileSource1);

    await indexFileSource1.transact(async t => {
        let result = await t.getFile(testPath, true)
        expect(result).toBe(testContent)
    })

    // Delete the file, this should be a full transaction.  
    await indexFileSource1.deleteFile(testPath)

    let data = await indexFileSource1.getFile(testPath, true)
    expect(data).toBe(null)

    const cachedJsonFileSource2 = new JSONMapFileSource(new CachedFileSource(mSource, { autoFlushing: false })) 
    const indexFileSource2 = new TransactionFileSource(cachedJsonFileSource2);
    data = await indexFileSource2.getFile(testPath, true);
    expect(data).toBe(null) 
}) 
