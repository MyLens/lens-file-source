import FileSource from '../../src/file-source/file-source';
import CachedFileSource from '../../src/file-source/cached-file-source';
import MockFileSource from '../mock-file-source';


test("Test the cached file source", async () => {
    // Perform some actions
    const performOps = async (s: FileSource) => { 
        await s.getFile("file1", false);
        await s.putFile("file1", "Some content", false);
        await s.putFile("file2", "Some other content", false);
        await s.getFile("file1", false);
        await s.getFile("file2", false);
        await s.getFile("file2", false);
        await s.getFile("file1", false);
        await s.getFile("file2", false);
        await s.putFile("file1", "Updated content", false);
        await s.getFile("file1", false);
        await s.putFile("file2", "Some other updated content", false);
        await s.getFile("file1", false);
        await s.getFile("file2", false);
        await s.getFile("file1", false);
        await s.listFiles();
        await s.getFile("file1", false);
        await s.getFile("file2", false);
        await s.deleteFile("file1");
        await s.getFile("file2", false);
        await s.deleteFile("file1");
    };

    // Perform directly on the mock.
    const mSource = new MockFileSource();
    await performOps(mSource);

    expect(mSource.getFileCalls).toBe(13);
    expect(mSource.putFileCalls).toBe(4);
    expect(mSource.deleteFileCalls).toBe(2);
    expect(mSource.listFilesCalls).toBe(1);

    // Perform cached version.
    const mSource2 = new MockFileSource();
    await performOps(new CachedFileSource(mSource2));

    expect(mSource2.getFileCalls).toBe(1);
    expect(mSource2.putFileCalls).toBe(4);
    expect(mSource2.deleteFileCalls).toBe(2);
    expect(mSource2.listFilesCalls).toBe(1);

    // Perform cached + flush version.
    const mSource3 = new MockFileSource();
    const cachedFileSource = new CachedFileSource(mSource3, { autoFlushing: false });
    await performOps(cachedFileSource);

    // Before flushing
    expect(mSource3.getFileCalls).toBe(1);
    expect(mSource3.putFileCalls).toBe(0);
    expect(mSource3.deleteFileCalls).toBe(0);
    expect(mSource3.listFilesCalls).toBe(1);

    await cachedFileSource.flush();

    // After flushing
    expect(mSource3.getFileCalls).toBe(1);
    expect(mSource3.putFileCalls).toBe(1);
    expect(mSource3.deleteFileCalls).toBe(1);
    expect(mSource3.listFilesCalls).toBe(1);
});
    
test("getFile on empty cache assigns cache", async () => {
    const mSource = new MockFileSource();

    await mSource.putFile("file1", "{'Hello':'World'}", false);

    const cachedFileSource = new CachedFileSource(mSource, { autoFlushing: false });
    const contents = await cachedFileSource.getFile("file1", false);
    expect(contents).toBe("{'Hello':'World'}");
})

test("putFile two or more times concurrently should execute safely", async () => {
    // concurrent puts on Mock should throw.
    let mSource = new MockFileSource();

    try {
        await Promise.all([
            mSource.putFile("file1", "Test 1", false),
            mSource.putFile("file1", "Test 2", false),
            mSource.putFile("file1", "Test 3", false),
            mSource.putFile("file1", "Test 4", false),
            mSource.putFile("file1", "Test 5", false),
        ]);
        fail();
    }
    catch(err) {
        expect(err).toBeTruthy();
    }

    // Reset mSource so we're not using a dirty/broken mock file source.
    mSource = new MockFileSource();
    const cachedFileSource = new CachedFileSource(mSource, { autoFlushing: false });
    await Promise.all([
        cachedFileSource.putFile("file1", "Test 1", false),
        cachedFileSource.putFile("file1", "Test 2", false),
        cachedFileSource.putFile("file1", "Test 3", false),
        cachedFileSource.putFile("file1", "Test 4", false),
        cachedFileSource.putFile("file1", "Test 5", false),
    ]);
    // Flushing should succeed, and should only 'put' the last one received.
    await cachedFileSource.flush("file1");

    const contents = await cachedFileSource.getFile("file1", false);
    expect(contents).toBe("Test 5");
})

test("flushing the same file 2 or more times concurrently should execute safely", async () => {
    // Don't bother testing failure on mock; there is no flush on mock.

    const mSource = new MockFileSource();
    const cachedFileSource = new CachedFileSource(mSource, { autoFlushing: false });
    await cachedFileSource.putFile("file1", "Test 1", false);
    await Promise.all([
        cachedFileSource.flush("file1"),
        cachedFileSource.flush("file1"),
        cachedFileSource.flush("file1"),
        cachedFileSource.flush("file1"),
        cachedFileSource.flush("file1")
    ]);
    
    const contents = await cachedFileSource.getFile("file1", false);
    expect(contents).toBe("Test 1");
});

test("getting the same file 2 or more times concurrently should only 'get' once", async () => {
    const mSource = new MockFileSource();

    // First add a file
    await mSource.putFile("file1", "Test 1", false);

    const cachedFileSource = new CachedFileSource(mSource, { autoFlushing: false });
    await Promise.all([
        cachedFileSource.getFile('file1', false),
        cachedFileSource.getFile('file1', false),
        cachedFileSource.getFile('file1', false),
        cachedFileSource.getFile('file1', false),
        cachedFileSource.getFile('file1', false)
    ]);

    expect(mSource.getFileCalls).toBe(1);
});


test("getting the same file URL 2 or more times concurrently should only 'getFileUrl' once", async () => {
    const mSource = new MockFileSource();

    // First add a file
    await mSource.putFile("file1", "Test 1", false);

    const cachedFileSource = new CachedFileSource(mSource, { autoFlushing: false });
    await Promise.all([
        cachedFileSource.getFileUrl('file1'),
        cachedFileSource.getFileUrl('file1'),
        cachedFileSource.getFileUrl('file1'),
        cachedFileSource.getFileUrl('file1'),
        cachedFileSource.getFileUrl('file1')
    ]);

    expect(mSource.getFileUrlCalls).toBe(1);
});

test("listing files 2 or more times concurrently should only 'listFiles' once", async () => {
    const mSource = new MockFileSource();

    // First add a file
    await mSource.putFile("file1", "Test 1", false);

    const cachedFileSource = new CachedFileSource(mSource, { autoFlushing: false });
    await Promise.all([
        cachedFileSource.listFiles(),
        cachedFileSource.listFiles(),
        cachedFileSource.listFiles(),
        cachedFileSource.listFiles(),
        cachedFileSource.listFiles()
    ]);

    expect(mSource.listFilesCalls).toBe(1);
});
