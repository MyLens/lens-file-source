import FileSource from '../../src/file-source/file-source';
import RandomAccessFileSource from '../../src/file-source/random-access-file-source';
import CachedFileSource from '../../src/file-source/cached-file-source';
import MockFileSource from '../mock-file-source';


test("Test the random access file source", async () => {
    // Perform some actions
    const performOps = async (s: FileSource) => { 
        await s.getFile("file1", false);
        await s.putFile("file1", "Some content", false);
        await s.putFile("file2", "Some other content", false);
        await s.getFile("file1", false);
        await s.getFile("file2", false);
        await s.getFile("file2", false);
        await s.getFile("file1", false);
        await s.getFile("file2", false);
        await s.putFile("file1", "Updated content", false);
        await s.getFile("file1", false);
        await s.putFile("file2", "Some other updated content", false);
        await s.getFile("file1", false);
        await s.getFile("file2", false);
        await s.getFile("file1", false);
        await s.listFiles();
        await s.getFile("file1", false);
        await s.getFile("file2", false);
        await s.deleteFile("file1");
        await s.getFile("file2", false);
        await s.deleteFile("file1");
    };

    // Perform directly on the mock.
    const mSource = new MockFileSource();
    await performOps(mSource);

    expect(mSource.getFileCalls).toBe(13);
    expect(mSource.putFileCalls).toBe(4);
    expect(mSource.deleteFileCalls).toBe(2);
    expect(mSource.listFilesCalls).toBe(1);

    // Perform RA version.
    const mSource2 = new MockFileSource();
    await performOps(new RandomAccessFileSource(new CachedFileSource(mSource2)));

    expect(mSource2.getFileCalls).toBe(1);
    expect(mSource2.putFileCalls).toBe(10); // More than default, because each change requires writing to master and to the file we save on.
    expect(mSource2.deleteFileCalls).toBe(0); // We never delete old empty files; we leave them there to be filled in again. 
    expect(mSource2.listFilesCalls).toBe(0); // List calls all read from the in-memory list read from the 'master' file.

    // Perform RA + flush version.
    const mSource3 = new MockFileSource();
    const cachedFileSource = new RandomAccessFileSource(new CachedFileSource(mSource3, { autoFlushing: false }));
    await performOps(cachedFileSource);

    // Before flushing
    expect(mSource3.getFileCalls).toBe(1); // 1 get for the master. we don't 'get' any other files, since they're only being created now, and will be in memory for us. 
    expect(mSource3.putFileCalls).toBe(0);
    expect(mSource3.deleteFileCalls).toBe(0);
    expect(mSource3.listFilesCalls).toBe(0);

    await cachedFileSource.flush();

    // After flushing
    expect(mSource3.getFileCalls).toBe(1);
    expect(mSource3.putFileCalls).toBe(2); // 1 saves: 1 for the master, 1 for the only file we're saving.
    expect(mSource3.deleteFileCalls).toBe(0);
    expect(mSource3.listFilesCalls).toBe(0);

    // Make sure values are correct.
    const mSource4 = new MockFileSource();
    const cachedFileSource2 = new RandomAccessFileSource(new CachedFileSource(mSource4, { autoFlushing: false }));
    await cachedFileSource2.putFile("file1", "The quick brown fox", false);
    let content = await cachedFileSource2.getFile("file1", false);
    expect(content).toBe("The quick brown fox");
    await cachedFileSource2.putFile("file2", "Brown bear, brown bear.", false);
    content = await cachedFileSource2.getFile("file2", false);
    expect(content).toBe("Brown bear, brown bear.");
    content = await cachedFileSource2.getFile("file1", false);
    expect(content).toBe("The quick brown fox");

    await cachedFileSource2.putFile("file1", "Cow jumps over the moon", false);
    content = await cachedFileSource2.getFile("file2", false);
    expect(content).toBe("Brown bear, brown bear.");
    content = await cachedFileSource2.getFile("file1", false);
    expect(content).toBe("Cow jumps over the moon");

    await cachedFileSource2.deleteFile("file2");
    content = await cachedFileSource2.getFile("file2", false);
    expect(content).toBe(null);
    content = await cachedFileSource2.getFile("file1", false);
    expect(content).toBe("Cow jumps over the moon");
});
    

test("putFile two or more times concurrently should execute safely", async () => {
    // concurrent puts on Mock should throw.
    let mSource = new MockFileSource();

    try {
        await Promise.all([
            mSource.putFile("file1", "Test 1", false),
            mSource.putFile("file1", "Test 2", false),
            mSource.putFile("file1", "Test 3", false),
            mSource.putFile("file1", "Test 4", false),
            mSource.putFile("file1", "Test 5", false),
        ]);
        fail();
    }
    catch(err) {
        expect(err).toBeTruthy();
    }

    // Reset mSource so we're not using a dirty/broken mock file source.
    mSource = new MockFileSource();
    const cachedFileSource = new RandomAccessFileSource(new CachedFileSource(mSource, { autoFlushing: false }));
    // We can/should be able to use different 'files' here, as they should write to the same master file.
    await Promise.all([
        cachedFileSource.putFile("file1", "Test 1", false),
        cachedFileSource.putFile("file2", "Test 2", false),
        cachedFileSource.putFile("file3", "Test 3", false),
        cachedFileSource.putFile("file4", "Test 4", false),
        cachedFileSource.putFile("file5", "Test 5", false),
    ]);
    // Flushing should succeed 
    await cachedFileSource.flush("file1");
    await cachedFileSource.flush("file2");
    await cachedFileSource.flush("file3");
    await cachedFileSource.flush("file4");
    await cachedFileSource.flush("file5");

    const contents = await cachedFileSource.getFile("file1", false);
    expect(contents).toBe("Test 1");
    const contents2 = await cachedFileSource.getFile("file5", false);
    expect(contents2).toBe("Test 5");
})

test("flushing the 'same' file 2 or more times concurrently should execute safely", async () => {
    // Don't bother testing failure on mock; there is no flush on mock.

    const mSource = new MockFileSource();
    const cachedFileSource = new RandomAccessFileSource(new CachedFileSource(mSource, { autoFlushing: false }));
    await cachedFileSource.putFile("file1", "Test 1", false);
    await cachedFileSource.putFile("file2", "Test 2", false);
    await cachedFileSource.putFile("file3", "Test 3", false);
    await cachedFileSource.putFile("file4", "Test 4", false);
    await cachedFileSource.putFile("file5", "Test 5", false);
    await Promise.all([
        cachedFileSource.flush("file1"),
        cachedFileSource.flush("file2"),
        cachedFileSource.flush("file3"),
        cachedFileSource.flush("file4"),
        cachedFileSource.flush("file5")
    ]);
    
    const contents = await cachedFileSource.getFile("file1", false);
    expect(contents).toBe("Test 1");
    const contents2 = await cachedFileSource.getFile("file5", false);
    expect(contents2).toBe("Test 5");
});

test("removing an existing item should still cause items to flush properly", async () => {

    // Populate some data.

    const mSource = new MockFileSource();
    const cachedFileSource = new RandomAccessFileSource(new CachedFileSource(mSource, { autoFlushing: false }));
    await cachedFileSource.putFile("file1", "Test 1", false);
    await cachedFileSource.flush("file1");

    // Get the Master file directly, so we can find out what file our RAFS is saving
    // content to.
    const masterEntry = JSON.parse((await mSource.getFile('ra-master.json', false)) as string)[0];

    // Ensure our mapping was correct.
    expect(await mSource.getFile(masterEntry.parentPath, false)).toBe("Test 1");

    // Recreate our file source from existing data.
    const cachedFileSource2 = new RandomAccessFileSource(new CachedFileSource(mSource, { autoFlushing: false }));

    // Ensure items still exist.
    const contents = await cachedFileSource2.getFile("file1", false);
    expect(contents).toBe("Test 1");

    // Removing item and flushing should remove properly.
    await cachedFileSource2.deleteFile("file1");
    await cachedFileSource2.flush("file1");

    // After deleting the only entry, expect our master to not contain it.
    expect(JSON.parse((await mSource.getFile('ra-master.json', false)) as string).length).toBe(0);
    // And expect the file to not contain the information anymore.
    expect(await mSource.getFile(masterEntry.parentPath, false)).not.toBe("Test 1");


    // Recreate our file source from existing data.
    const cachedFileSource3 = new RandomAccessFileSource(new CachedFileSource(mSource, { autoFlushing: false }));

    // Ensure item does not exist.
    try {
        const contents = await cachedFileSource3.getFile("file1", false);
    } catch(err) {
        expect(err).toBeTruthy();
    }
});