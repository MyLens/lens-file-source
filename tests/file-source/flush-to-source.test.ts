import MockFileSource from "../mock-file-source";
import JSONMapFileSource from "../../src/file-source/json-map-file-source";
import CachedFileSource from "../../src/file-source/cached-file-source";
import TransactionFileSource from "../../src/file-source/transaction-file-source"

test("Ensure data persists if cache is cleared", async () => {
    const mSource = new MockFileSource(); 
    const cachedJsonFileSource = new JSONMapFileSource(new CachedFileSource(mSource, { autoFlushing: false })) 
    const cachedJsonFileSource1 = new JSONMapFileSource(new CachedFileSource(mSource, { autoFlushing: false })) 
    const indexFileSource = new TransactionFileSource(cachedJsonFileSource);
    const indexFileSource1 = new TransactionFileSource(cachedJsonFileSource1);

    const testPath = "some_data" 
    const testContent = "Some content"
    // This should be flushed to the source
    await indexFileSource.transact(async t => {
        await t.putFile(testPath, testContent, true); 
    })

    // Make sure that the correct value is in the other file source. 
    // This ensures that the data was actually written to the datastore. 
    await indexFileSource1.transact(async t => {
        let result = await t.getFile(testPath, true)
        expect(result).toBe(testContent)
    })
}) 
