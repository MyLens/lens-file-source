import Mutex from '../src/mutex'


function delay(time: number): Promise<void> {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve();
        }, time);
    })
}

test("Mutex runs one", async () => {
    const m = new Mutex();

    const original= "hi";
    const newValue = "bye";
    let value = original;

    await m.lock(async () => { value = newValue; });

    expect(value).toBe(newValue);
    
    const ret = await m.lock(async () => { return original; });

    expect(ret).toBe(original);
});

test("Mutex waits", async () => {
    const m = new Mutex();

    const original= "hi";
    const newValue = "bye";
    let value = original;
    
    await Promise.all([
        m.lock(async () => { await delay(100); value = "bad"; }),
        m.lock(async () => { await delay(50); value = newValue; })
    ]);

    expect(value).toBe(newValue);
});

test("All mutex requests are executed in order", async () => {
    const m = new Mutex();

    const original= "hi";
    const newValue = "bye";
    let value = original;
    const order : number[] = [];

    await Promise.all([
        m.lock(async () => { await delay(100); value = "bad"; order.push(1); }),
        m.lock(async () => { await delay(55); value = "bad";  order.push(2); }),
        m.lock(async () => { await delay(2); value = "bad";  order.push(3); }),
        m.lock(async () => { await delay(42); value = "bad";  order.push(4); }),
        m.lock(async () => { await delay(73); value = "bad";  order.push(5); }),
        m.lock(async () => { await delay(50); value = newValue;  order.push(6); })
    ]);

    expect(value).toBe(newValue);
    expect(order[0]).toBe(1);
    expect(order[1]).toBe(2);
    expect(order[2]).toBe(3);
    expect(order[3]).toBe(4);
    expect(order[4]).toBe(5);
    expect(order[5]).toBe(6);
});

test("Mutex forwards thrown exceptions", async () => {
    const m = new Mutex();

    const original= "hi";
    const newValue = "bye";
    let value = original;
    const order : number[] = [];

    const reqs = [
        m.lock(async () => { await delay(100); value = "bad"; order.push(1); }),
        m.lock(async () => { await delay(55); value = "bad";  throw new Error("Problem!"); }),
        m.lock(async () => { await delay(2); value = "bad";  order.push(3); }),
        m.lock(async () => { await delay(42); value = "bad";  order.push(4); }),
        m.lock(async () => { await delay(73); value = "bad";  order.push(5); }),
        m.lock(async () => { await delay(50); value = newValue;  order.push(6); })
    ];

    for(let x=0;x<reqs.length;x++) {
        const req = reqs[x];
        if(x == 1) {
            try {
                await req;
                fail();
            } catch(err) {
                console.log(err);
                expect(err).toBeTruthy()
            }
        } else {
            await req;
        }
    }

    expect(value).toBe(newValue);
});