# lens-file-source 

File storage abstraction through mechanisms like caching, transactions and JSON map aggregation.

# To Use with Gaia #

You can adapt as needed to your own implementation, but at the minimum you'll need
a GaiaFileSource, to manage writing and reading to and from Gaia as a FileSource.

We do this by first redefining a subset of the Gaia UserSession:
```ts

// ./session-adapter.ts

export interface PutFileOptions {
    encrypt : boolean | string, 
    sign: boolean | string
}

export interface GetFileOptions {
    decrypt: boolean, 
    verify: boolean
}

export interface UserData {
    appPrivateKey: string, 
    username: string, 
    identityAddress: string,
    profile: any
}

export default interface SessionAdapter  {
    putFile(path: string, content: string, options?: PutFileOptions): Promise<string>
    deleteFile(path: string ) : Promise<void>   
    getFile(path: string, options?: GetFileOptions): Promise<string | null>
    getFileUrl(path: string) : Promise<string>
    loadUserData() : UserData
    encryptContent(content: string | Buffer, options?: {publicKey?: string}) : string
    decryptContent(content: string, options?: object): string | Buffer
}
```

We then define a GaiaFileSource:
```ts
// ./gaia-file-source.ts

import SessionAdapter from './session-adapter';
import { FileSource } from '@mylens/lens-file-source';

export default class GaiaFileSource implements FileSource {
    private sessionAdapter: SessionAdapter;
    constructor(sessionAdapter: SessionAdapter) {
        this.sessionAdapter = sessionAdapter;
    }
    async getFile(path: string, decrypt: boolean): Promise<string | null> { return await this.sessionAdapter.getFile(path, { decrypt: decrypt, verify: false }); }
    async putFile(path: string, content: string, encrypt: boolean): Promise<void> { await this.sessionAdapter.putFile(path, content, { encrypt: encrypt, sign: false }); }
    async deleteFile(path: string) : Promise<void> { await this.sessionAdapter.deleteFile(path); }
    async listFiles(options?: { pathFilter?: string, callback?: (name: string) => boolean }): Promise<Array<string>> { throw new Error("Not implemented"); }
    async getFileUrl(path: string): Promise<string | null> { return await this.sessionAdapter.getFileUrl(path); }
}
```

Once we have the file source, we can use it in whatever configuration we choose:
```
import { UserSession } from 'blockstack/lib/auth/userSession';
import GaiaFileSource from './gaia-file-source'

const userSession : SessionAdapter = new UserSession({ appConfig: new AppConfig(['store_write', 'publish_data'])}); 
const cachingFS = new CachedFileSource(new GaiaFileSource(userSession))
```
