
interface Request {
    resolve: (value?: {} | PromiseLike<{}> | undefined) => void,
    reject: (reason?: any) => void,
    task: () => Promise<any>
}

export default class Mutex {
    private isLocked = false;
    private waiting: Array<Request> = [];
    
    private perform(request: Request) {
        request.task().then((value) => {
            if(setImmediate) setImmediate(this.runNext.bind(this));
            else process.nextTick(this.runNext.bind(this));

            request.resolve(value);
        })
        .catch((err) => {
            if(setImmediate) setImmediate(this.runNext.bind(this));
            else process.nextTick(this.runNext.bind(this));

            request.reject(err);
        });
    }

    private runNext() {
        const next = this.waiting.shift();
        if(next) this.perform(next);
        else this.isLocked = false;
    }

    async lock(task: () => Promise<any>) : Promise<any> {
        return new Promise((resolve, reject) => {
            this.waiting.push({
                resolve: resolve,
                reject: reject,
                task: task
            });

            if(!this.isLocked) {
                this.isLocked = true;

                this.runNext();
            }
        })
    }
}